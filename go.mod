module github.com/milodarka/go_login

go 1.17

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	gorm.io/driver/sqlite v1.2.4
	gorm.io/gorm v1.22.2
)

require (
	github.com/gorilla/securecookie v1.1.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.2 // indirect
	github.com/mattn/go-sqlite3 v1.14.9 // indirect
)
