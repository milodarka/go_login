package main

import(
	"net/http"
	"github.com/milodarka/go_login/pkg/routes"
	"github.com/gorilla/mux"
	
	_ "github.com/go-sql-driver/mysql"
	
)

func  main()  {
	r := mux.NewRouter()
	routes.RegisterUserRoutes(r)
	http.Handle("/", r)
	http.ListenAndServe(":3000", r)
	
}

// http.HandleFunc("/index", accountcontroller.Index)
// 	http.HandleFunc("/welcome", accountcontroller.Welcome)
// 	http.HandleFunc("/login", accountcontroller.Login)
// 	http.HandleFunc("/logout", accountcontroller.Logout)
// 	http.HandleFunc("/register", accountcontroller.Register)
// 	http.ListenAndServe(":3000", nil)