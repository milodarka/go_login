package routes

import (
	"github.com/gorilla/mux"
	"github.com/milodarka/go_login/src/controllers/accountcontroller"
)
var RegisterUserRoutes = func(router *mux.Router){
	router.HandleFunc("/index", accountcontroller.Index).Methods("GET")
	router.HandleFunc("/register",accountcontroller.Register).Methods("POST")
	router.HandleFunc("/login", accountcontroller.Index).Methods("GET")
	router.HandleFunc("/logout", accountcontroller.Logout).Methods("GET")
	router.HandleFunc("/login-user", accountcontroller.Login).Methods("POST")
	router.HandleFunc("/welcome", accountcontroller.Welcome).Methods("GET")
	// router.HandleFunc("/something/{somethingiID", contollers.UpdateSomething).Methods("PUT")
	// router.HandleFunc("/something/{somethingidID}", contollers.DeleteSomething).Methods("DELETE")
}