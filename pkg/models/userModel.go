package models

import (
	"gorm.io/gorm"
	"github.com/milodarka/go_login/config"
	"time"
	
	
)

var db * gorm.DB

type User struct {
	ID	int	`gorm:"primary_key" json:"_id"`
	First_name 	*string	`json: "first_name" validate:"requiered, min=2, max=100"`
	Last_name *string	`json: "last_name" validate:"requiered, min=2, max=100"`
	Username *string	`json: "username" validate:"requiered, min=2, max=100"`
	Password *string	`json: "password" validate:"required, min=6"`
	Email *string	`json: "email" validate:"email,requiered"`
	Phone *string	`json:"phone validate:"requiered"`
	Token  *string	`json:"token"`
	Active bool
	User_type	*string	`json:"user_type" validate:"requiered, eq=ADMIN|eq=USER"`
	Refresh_token	 *string	`json:"refres_token"`
	Created_at	time.Time	`json:"created_at`
	Updated_at	time.Time	`json:"updated_at"`
	User_id	 *string	`json:"user_id"`
}

func init(){
	config.Connect()
	db = config.GetDB()
	db.AutoMigrate(&User{})
}

func (s *User) CreateUser() *User {
	//db.NewRecord(s)
	db.Create(&s)
	return s
}

// func GetAllUser() []User{
// 	var user []User
// 	db.Find(&User)
// 	return user
// }

func GetUserById(Id int64) (*User, *gorm.DB){
	var getUser User
	db := db.Where("ID=?", Id).Find(&getUser)
	return &getUser, db
}

// func DeleteUser(Id int64) User{
// 	var user User
// 	db.Where("ID=?", Id).Delete(User)
// 	return user
// }