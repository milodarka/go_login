package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"fmt"
)
// var (
// 	dbConn * db.DB
// )

func Connect() {
	connectionString := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s", "root", "","localhost", "test_1")
	db, err := sql.Open("mysql", connectionString)
	
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	fmt.Print("Succesfully connected to database")
}
// func GetDB() * db.DB {
// 	return dbConn
// }
func main(){
	Connect()
}
